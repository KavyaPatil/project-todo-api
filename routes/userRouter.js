const express = require("express");
const router = express.Router();
const {
  getUserController,
  addUserController,
  updateUserController
} = require("../controllers/userController");

router.post("/", addUserController);

router.get("/:username", getUserController);

router.put("/:username", updateUserController);

module.exports = router;
