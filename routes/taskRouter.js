const express = require("express");
const router = express.Router();
const task = require("../controllers/taskController");

router.get("/api/project/:projectId/tasks", task.getTasksController);

router.post("/api/project/:projectId/tasks", task.addTaskContoller);

router.get("/api/project/:projectId/tasks/:taskId", task.getTaskByIdController);

router.put("/api/project/:projectId/tasks/:taskId", task.updateTaskController);

router.delete("/api/project/:projectId/tasks/:taskId", task.deleteTaskController);

module.exports = router;
