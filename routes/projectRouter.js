const express = require("express");
const router = express.Router();
const project = require("../controllers/projectController");

router.get("/", project.getProjectsController);

router.post("/", project.addProjectContoller);

router.get("/:id", project.getProjectByIdController);

router.put("/:id", project.updateProjectController);

router.delete("/:id", project.deleteProjectController);

module.exports = router;
