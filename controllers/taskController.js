const pool = require("../databse/db");

const getTasksController = async (req, res) => {

  const projectId = parseInt(req.params.projectId);
  // console.log(req.params);

  try {
    
    const query = `SELECT * FROM tasks WHERE project_id = $1`;
    const result = await pool.query(query, [projectId]);
    res.json(result.rows);
  } catch (err) {
    console.log(err);
    res.status(400).send("Having problem with getting the tasks");
  }
};

const addTaskContoller = async (req, res) => {
  const projectId = parseInt(req.params.projectId);
  const { task_description, status } = req.body;
console.log(projectId, task_description, status)
  try {
    const query = `INSERT INTO tasks (project_id, task_description, status) VALUES ($1, $2, $3) RETURNING *`;
    const result = await pool.query(query, [
      projectId,
      task_description,
      status,
    ]);
    res.status(200).json(result.rows[0]);
  } catch (err) {
    console.log(err);
    res.status(400).send("Having problem with adding the task");
  }
};

const getTaskByIdController = async (req, res) => {
  const projectId = parseInt(req.params.projectId);
  const taskId = parseInt(req.params.taskId);

  try {
    const query = `SELECT * FROM tasks WHERE project_id = $1 AND task_id = $2`;
    const result = await pool.query(query, [projectId, taskId]);
    res.status(200).json(result.rows[0]);
  } catch (err) {
    console.log(err);
    res.status(400).send("Having problem with getting the task");
  }
};

const updateTaskController = async (req, res) => {
  const projectId = parseInt(req.params.projectId);
  const taskId = parseInt(req.params.taskId);

  const { task_description, status } = req.body;

  try {
    const query = `UPDATE tasks SET task_description = $1, status = $2 WHERE project_id = $3 AND task_id = $4 RETURNING *`;
    const result = await pool.query(query, [
      task_description,
      status,
      projectId,
      taskId,
    ]);
    res.status(200).json(result.rows);
  } catch (err) {
    console.log(err);
    res.status(400).send("Having problem with updating the task");
  }
};

const deleteTaskController = async (req, res) => {
  const projectId = parseInt(req.params.projectId);
  const taskId = parseInt(req.params.taskId);

  try {
    const query = `DELETE FROM tasks WHERE project_id = $1 AND task_id = $2 `;
    const result = await pool.query(query, [projectId, taskId]);
    res.status(200).send("deleted the data successfully");
  } catch (err) {
    console.log(err);
    res.status(400).send("Having problem with deleting the task");
  }
};

module.exports = {
  getTasksController,
  addTaskContoller,
  getTaskByIdController,
  updateTaskController,
  deleteTaskController,
};
