const pool = require("../databse/db");

const getUserController = async (req, res) => {
  const username = req.params.username;
  try {
    const query = `SELECT * FROM users WHERE username = ($1)`;
    const result = await pool.query(query, [username]);
    res.json(result.rows);
  } catch (err) {
    console.log(err);
    res.status(400).send("Having problem with getting the user details");
  }
};

const addUserController = async (req, res) => {
  const { name, email } = req.body;
  try {
    if (!name || !email) {
      return res.status(400).send("Provide Required details");
    } else {
      const query = `INSERT INTO users (username, email) VALUES ('${name}','${email}')`;
      const result = await pool.query(query);
      res.status(200).json(result.rows[0]);
    }
  } catch (err) {
    console.log(err);
    res.status(400).send("Having problem with adding the user details");
  }
};

const updateUserController = async (req, res) => {
  const { name, email } = req.body;
  try {
    const query = `UPDATE users SET username=$1, email=$2 VALUES ('${name}','${email}')`;
    const result = await pool.query(query);
    res.status(200).json(result.rows[0]);
  } catch (err) {
    console.log(err);
    res.status(400).send("Having problem with adding the user details");
  }
};

module.exports = {
  getUserController,
  addUserController,
  updateUserController,
};
