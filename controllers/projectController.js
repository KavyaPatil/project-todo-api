const pool = require("../databse/db");

const getProjectsController = async (req, res) => {

  const { user_id } = req.body;
 
  try {
    const query = 'SELECT * FROM projects WHERE user_id = $1';
    const result = await pool.query(query, [user_id]);
    console.log(result)
    res.status(200).json(result.rows);
  } catch (err) {
    console.log(err);
    res.status(400).send("Having problem with getting the projects");
  }
};

const addProjectContoller = async (req, res) => {

  const { title, description, user_id, project_status } = req.body;

  try {
    const query = `INSERT INTO projects (title, description, user_id, project_status) VALUES ($1, $2, $3, $4) RETURNING *`;
    const result = await pool.query(query, [
      title,
      description,
      user_id,
      project_status,
    ]);
    res.status(200).json(result.rows[0]);
  } catch (err) {
    console.log(err);
    res.status(400).send("Having problem with adding the project");
  }
};

const getProjectByIdController = async (req, res) => {

  const id = parseInt(req.params.id);
  console.log(req.params.id);

  try {
    const query = `SELECT * FROM projects WHERE project_id = $1`;
    const result = await pool.query(query, [id]);
    res.status(200).json(result.rows[0]);
  } catch (err) {
    console.log(err);
    res.status(400).send("Having problem with getting the project");
  }
};

const updateProjectController = async (req, res) => {

  const id = parseInt(req.params.id);
  const { title, description } = req.body;

  try {
    const query = `UPDATE projects SET title = $1, description = $2 WHERE project_id = $3 RETURNING *`;
    const result = await pool.query(query, [title, description, id]);
    res.status(200).json(result.rows);
  } catch (err) {
    console.log(err);
    res.status(400).send("Having problem with updating the projects");
  }
};

const deleteProjectController = async (req, res) => {

  const id = parseInt(req.params.id);

  try {
    const query = `DELETE FROM projects WHERE project_id = $1`;
    const result = await pool.query(query, [id]);
    res.status(200).send("deleted the data successfully");
  } catch (err) {
    console.log(err);
    res.status(400).send("Having problem with deleting the projects");
  }
};

module.exports = {
  getProjectsController,
  addProjectContoller,
  getProjectByIdController,
  updateProjectController,
  deleteProjectController,
};
