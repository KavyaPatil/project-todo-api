const express = require("express");
const app = express();
const userRouter = require("./routes/userRouter");
const projectRouter = require("./routes/projectRouter");
const tasksRouter = require('./routes/taskRouter')
const errorHandlerMiddlewae = require("./controllers/errorHandler");
require("dotenv").config();

const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// app.get("/", (req, res) => {
//   res.status(200).json({ message: "Hello World" });
// });

app.use("/api/user", userRouter);

app.use("/api/project", projectRouter);

app.use(tasksRouter);


app.listen(PORT, () => {
  console.log(`Server is live on port ${PORT}`);
});
